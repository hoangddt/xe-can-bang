#include "MotorController.h"

void registerWheel(Wheel *a)
{
  pinMode(a->pin1, OUTPUT);
  pinMode(a->pin2, OUTPUT);
  pinMode(a->speedPin, OUTPUT);
}

void executeWheel(Wheel *a)
{
  digitalWrite(a->pin1, a->pin1Value);
  digitalWrite(a->pin2, a->pin2Value);
  analogWrite(a->speedPin, a->speedValue);
}

void wheelGoUp(Wheel *w)
{
  w->pin1Value = HIGH;
  w->pin2Value = LOW;
  executeWheel(w);
}

void wheelGoDown(Wheel *w)
{
  w->pin1Value = LOW;
  w->pin2Value = HIGH;
  executeWheel(w);
}

void stopWheel(Wheel *w)
{
  w->pin1Value = LOW;
  w->pin2Value = LOW;
  executeWheel(w);
}

void reverseWheel(Wheel *w)
{
  w->pin1Value = 1 - w->pin1Value;
  w->pin2Value = 1 - w->pin2Value;
  executeWheel(w);
}

void setSpeedByPercent(Wheel *w, byte speed)
{
  speed = map(speed, 0, 100, 0, 255);
  setSpeedByPWN(w, speed);
}

void setSpeedByPWN(Wheel *w, byte speed)
{
  if (speed < 0)  speed = 0;
 if (speed > 255) speed = 255;
  w->speedValue = speed;
}
