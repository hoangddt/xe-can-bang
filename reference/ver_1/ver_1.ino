#include "I2Cdev.h"
#include <MsTimer2.h>
#define CHA 3
#define CHB 4

#include "MPU6050_6Axis_MotionApps20.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

MPU6050 mpu;

#define OUTPUT_READABLE_YAWPITCHROLL

bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// packet structure for InvenSense teapot demo
uint8_t teapotPacket[14] = { '$', 0x02, 0,0, 0,0, 0,0, 0,0, 0x00, 0x00, '\r', '\n' };

//pwm pin
const int analogOutPin1 = 5;
const int analogOutPin2 = 9;
//**********************

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}
float previous_error = 0; 
float dt=0.005;
float actual_position=0;
float error;
float setpoint=6;
const float SPB=6;
float integral=0,derivative;
//float Kp=21,Ki=84,Kd=1.3125;
float Kp,Ki,Kd;
float Ku=30,Pu=0.22;
float output;
float count,count_S=0,error_S,count_T;
float KpS=0.02,KiS=0.006,KdS=0.00032;//he so thay doi goc
float KuS=0.00017,PuS=50;
float I_S,D_S;
float Pre_error_S;
int mode,func;
float setpoint_temp;
byte xoay=0;// xoay=0 ko quay, xoay=1 quay trai, xoay=2 quay phai
float offset_left,offset_right;
boolean chieu,nga;

void rate()
{
  mpuInterrupt = true;
  
  actual_position=ypr[2] * 180/M_PI;
  error = setpoint - actual_position;
  integral = integral + (error*dt);
  derivative = (error - previous_error)/dt;
  output = (Kp*error) + (Ki*integral) + (Kd*derivative);

  
  //Serial.println(output);  
  if(output>=255) output=255;
    if(output<=-255) output=-255;
    
    
  previous_error = error;

  if((xoay==0)||(nga==1))
  {
    if(output < 0) 
    {
      output=abs(output);
      digitalWrite(7, 0);
      digitalWrite(8, 0);
      //chieu=0;
    }
    else
      {
        digitalWrite(7, 1);
        digitalWrite(8, 1);
        //chieu=1;  
      }
    analogWrite(analogOutPin2,(int)output);
    if(output<(255-10))output+=10;
    analogWrite(analogOutPin1,(int)output);
    
    
  }
  else if((xoay==1)&&(nga==0)){
    analogWrite(analogOutPin1, (int)offset_left);
    analogWrite(analogOutPin2, (int)offset_left);
    Serial.println("RE trai");
  }
  else if((xoay==2)&&(nga==0)){
    analogWrite(analogOutPin1, (int)offset_right);
    analogWrite(analogOutPin2, (int)offset_right);
  }
  
  
  ////////////////////////////////////
  //pid vi tri
  if(xoay==0)
  {
    error_S=-(count_S-count);
    I_S=I_S+(error_S*dt);
    if(I_S>320) I_S=320;
      else if(I_S<-200) I_S=-200;
    //I_S=0;
    D_S=(error_S-Pre_error_S)/dt;
  
    setpoint=(error_S*KpS)+(KiS*I_S)+(KdS*D_S)+SPB;
    //setpoint=-3;
    setpoint_temp=setpoint;
    
    if(setpoint<-15) setpoint=-15;
      else if(setpoint>15
      ) setpoint=15; 
    Pre_error_S=error_S;  
  }
  
}

void encoder()
  {
    if(digitalRead(CHB)==0)
      count-=1;
      else count+=1;
      //Serial.println(count);
    }


void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    Serial.println(inChar);
    
      if(inChar== 'f'){ 
                        //func=1;
                        //mode=0;
                        count_S+=100;
                        Serial.println(F("Tien"));
                        }// Tien
      if(inChar== 'b'){ 
                        //func=2;
                        //mode=0;
                        count_S-=100;
                        Serial.println(F("Lui"));
                        }// Lui
       if(inChar=='l')
                      {
                        count_T=count;
                        xoay=1;             
                      }// re trai
        if(inChar=='r')
                      {
                        count_T=count;
                        xoay=2;             
                      }// re phai
        if(inChar=='s')
                      {
                        xoay=0;
                        count_S=count;
                        offset_left=0;
                        }
    }
  }
void turn_left(int STL)
{
  float Kq=0.8;
  if(abs(count_T-count)>STL)
    {
      xoay=0;
      offset_left=0;
      count_S=count;
    }
  else 
    {
    if((abs(ypr[2] * 180/M_PI-SPB)<1))
      {
        nga=0;
        digitalWrite(7, 0);
        digitalWrite(8, 1);
        offset_left=abs(count_T-count)*Kq;
        if(offset_left<40) offset_left=40;
      }

    else 
      {
        offset_left=0;
        count_S=count;
        setpoint=SPB;
        nga=1;
      }
    }
            
}
void turn_right(int STR)
{
  float Kq=0.7;
  if(abs(count_T-count)>STR)
    {
      xoay=0;
      offset_right=0;
      count_S=count;
    }
  else 
    {
    if((abs(ypr[2] * 180/M_PI-SPB)<1))
      {
        nga=0;
        digitalWrite(7, 1);
        digitalWrite(8, 0);
        offset_right=abs(count_T-count)*Kq;
        if(offset_right<40) offset_right=40;
      }
    else 
      {
        offset_right=0;
        count_S=count;
        setpoint=SPB;
        nga=1;
      }
    }        
}

void setup() {
  //tinh he so toc do
  Kp=0.6*Ku;
  Ki=2*(Kp/Pu);
  Kd=(Kp*Pu)/8;
  
  //Tinh he so vi tri
  /*KpS=0.6*KuS;
  KiS=2*(KpS/PuS);
  KdS=(KpS*PuS)/8;*/
  pinMode(7, OUTPUT);
  digitalWrite(7, 0);
  analogWrite(analogOutPin1, 0);
  pinMode(8, OUTPUT);
  digitalWrite(8, 0);
  analogWrite(analogOutPin2, 0);

  pinMode(CHA, INPUT_PULLUP);
  pinMode(CHB, INPUT_PULLUP);
  
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    Serial.begin(115200);
    while (!Serial);

    mpu.initialize();

    devStatus = mpu.dmpInitialize();

    mpu.setXGyroOffset(220);
    mpu.setYGyroOffset(76);
    mpu.setZGyroOffset(-85);
    mpu.setZAccelOffset(1788);

    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
        attachInterrupt(0, dmpDataReady, RISING);
        
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }
    //Thiet lap timer
    MsTimer2::set(5, rate); // 5ms period
    MsTimer2::start();

    attachInterrupt(1, encoder, RISING);

    
}

void loop() {
  if (!dmpReady) return;

  while (!mpuInterrupt && fifoCount < packetSize) {

    }

    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    fifoCount = mpu.getFIFOCount();
/////////////////////////
    if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & 0x02) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        #ifdef OUTPUT_READABLE_YAWPITCHROLL
            // display Euler angles in degrees
            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetGravity(&gravity, &q);
            mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
            //Serial.print("ypr\t");
            //Serial.print(ypr[0] * 180/M_PI);
            //Serial.print("\t");
            //Serial.print();
            /*Serial.print("count_S  ");
            Serial.print(count_S);
            Serial.print("  count  ");
            Serial.print(count);
            Serial.print("  error_S  ");
            Serial.print(error_S);
            Serial.print("  setpoint  ");
            Serial.print(setpoint);
            Serial.print(" KPS ");
            Serial.print(KpS*100);
            Serial.print("  KIS  ");
            Serial.print(KiS*100);
            Serial.print("  KDS  ");
            Serial.print(KdS*100);
            Serial.print("  p  ");
            Serial.print(ypr[1] * 180/M_PI);
            Serial.print(" PS ");
            Serial.print(KpS*error);
            Serial.print("  IS  ");
            Serial.print(I_S);
            Serial.print("  DS  ");
            Serial.println(D_S);*/
            //Serial.print("  error  ");
            //Serial.print(abs(count_T-count));
            Serial.print("  goc= ");
            Serial.print((ypr[2] * 180/M_PI));
            Serial.print("  error_S  ");
            Serial.print(error_S);     
            Serial.print("   ");
            Serial.print(setpoint);
            Serial.print("   ");
            Serial.print(derivative);
            Serial.print("   ");
            Serial.println(output);
            //Serial.println(abs(ypr[1]+SPB));       
            /*Serial.print("  p  ");
            Serial.println(ypr[1] * 180/M_PI);*/
        #endif

        if(xoay==1)
          turn_left(10);
          else if (xoay==2)turn_right(10);
    }

   }
